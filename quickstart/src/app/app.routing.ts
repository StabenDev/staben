import { Router, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent} from './about/about.component';
import { ProjectsComponent} from './projects/projects.component';
// import { NavbarComponent} from './navbar/navbar.component';

export const routing = RouterModule.forRoot([
    {path: '', component: HomeComponent},
    {path: 'projects', component: ProjectsComponent},
    {path: 'about', component: AboutComponent},
]);