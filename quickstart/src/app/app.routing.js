"use strict";
var router_1 = require("@angular/router");
var home_component_1 = require("./home/home.component");
var about_component_1 = require("./about/about.component");
var projects_component_1 = require("./projects/projects.component");
// import { NavbarComponent} from './navbar/navbar.component';
exports.routing = router_1.RouterModule.forRoot([
    { path: '', component: home_component_1.HomeComponent },
    { path: 'projects', component: projects_component_1.ProjectsComponent },
    { path: 'about', component: about_component_1.AboutComponent },
]);
//# sourceMappingURL=app.routing.js.map