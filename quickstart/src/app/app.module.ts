import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { AboutComponent }  from './about/about.component';
import { HomeComponent} from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProjectsComponent} from './projects/projects.component';
import { routing } from './app.routing';


@NgModule({
  imports:      [ BrowserModule,
                  routing,
                ],
  declarations: [ AppComponent,
                  AboutComponent, 
                  HomeComponent, 
                  NavbarComponent,
                  ProjectsComponent,
                ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
